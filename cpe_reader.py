from zipfile import ZipFile
from cpe_dowloader import CPE_DICT_FILE_PATH, download_dict
import xml.etree.ElementTree as xml_parser
import sys

TYPE_KEY = 'type'
NAME_KEY = 'name'
DEPRECATED_BY_KEY = 'deprecated-by'
DEPRECATION_KEY = 'deprecation'
CPE_KEY = 'cpe23-item'
NAMESPACE = '{http://scap.nist.gov/schema/cpe-extension/2.3}'
DEPRECATED_KEY = 'deprecated'
ZIP_EXTENSION = '.zip'


def read_cpe_entries():
    cpe_dict = unzip_cpe_dict()
    cpe_dict_xml = xml_parser.parse(cpe_dict)
    return cpe_dict_xml.getroot()


def get_cpe_entries_names(cpe_entries):
    cpe_entries_names = []
    for cpe_entry in cpe_entries:
        if (cpe_entry.attrib is not None) and (NAME_KEY in cpe_entry.attrib):
            cpe_entries_names.append(cpe_entry.attrib[NAME_KEY])
    return cpe_entries_names


def unzip_cpe_dict():
    with ZipFile(CPE_DICT_FILE_PATH) as feed_zip:
        return feed_zip.open(get_cpe_dit_filename())


def get_cpe_dit_filename():
    filename_with_zip_ext = CPE_DICT_FILE_PATH.split("\\")
    return filename_with_zip_ext[filename_with_zip_ext.__len__() - 1].replace(ZIP_EXTENSION, '')


def get_deprecated_cpe_entries(cpe_entries):
    deprecated_cpe_entries = []
    for cpe_entry in cpe_entries:
        cpe_entry_attrib = cpe_entry.attrib
        if is_deprecated(cpe_entry_attrib):
            deprecated_cpe = create_deprecated_cpe_entry_json(cpe_entry)
            deprecated_cpe_entries.append(deprecated_cpe)
    return deprecated_cpe_entries


def is_deprecated(cpe_entry_attrib):
    return (cpe_entry_attrib is not None) and (DEPRECATED_KEY in cpe_entry_attrib) \
           and cpe_entry_attrib[DEPRECATED_KEY] == 'true'


def create_deprecated_cpe_entry_json(cpe_entry):
    deprecated_by = get_deprecated_by(cpe_entry)
    return {DEPRECATED_KEY: cpe_entry.attrib[NAME_KEY],
            DEPRECATED_BY_KEY: deprecated_by[NAME_KEY],
            TYPE_KEY: deprecated_by[TYPE_KEY]}


def get_deprecated_by(cpe_entry):
    return cpe_entry.find('%s%s' % (NAMESPACE, CPE_KEY)).find('%s%s' % (NAMESPACE, DEPRECATION_KEY)).find(
        '%s%s' % (NAMESPACE, DEPRECATED_BY_KEY)).attrib


def main():
    download_dict()
    cpe_entries = read_cpe_entries()
    deprecated_cpe_entries = get_deprecated_cpe_entries(cpe_entries)
    print(*deprecated_cpe_entries, sep='\n')


if __name__ == '__main__':
    sys.exit(main())
