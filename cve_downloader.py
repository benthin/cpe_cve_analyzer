import os
import sys
import urllib.request as downloader

CVE_FEEDS_URL = 'https://nvd.nist.gov/feeds/xml/cve/2.0/'
CVE_FEED_FILE_PREFIX = 'nvdcve-2.0-'
CVE_FEED_FILE_SUFFIX = '.xml.zip'
CVE_FEEDS_DOWNLOAD_DIR = os.path.join(os.path.curdir, 'cve_feeds')
CVE_MODIFIED_FEED = 'modified'
CVE_FEEDS_START_YEAR = 2002
CVE_FEEDS_END_YEAR = 2018


def download_feeds():
    create_download_dir()
    download_modified_feed()
    for year in range(CVE_FEEDS_START_YEAR, CVE_FEEDS_END_YEAR + 1):
        feed_file = CVE_FEED_FILE_PREFIX + str(year) + CVE_FEED_FILE_SUFFIX
        download_feed(feed_file)


def download_modified_feed():
    feed_file = CVE_FEED_FILE_PREFIX + CVE_MODIFIED_FEED + CVE_FEED_FILE_SUFFIX
    download_feed(feed_file)


def create_download_dir():
    if not os.path.isdir(CVE_FEEDS_DOWNLOAD_DIR):
        os.mkdir(CVE_FEEDS_DOWNLOAD_DIR)


def download_feed(feed_file):
    if not feed_exists(feed_file):
        print("Downloading " + feed_file)
        url = CVE_FEEDS_URL + feed_file
        file_path = os.path.join(CVE_FEEDS_DOWNLOAD_DIR, feed_file)
        downloader.urlretrieve(url, file_path)


def feed_exists(feed_file):
    return os.path.isfile(os.path.join(CVE_FEEDS_DOWNLOAD_DIR, feed_file))


def main():
    download_feeds()


if __name__ == '__main__':
    sys.exit(main())

