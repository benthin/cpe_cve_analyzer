from zipfile import ZipFile
from cve_downloader import CVE_FEEDS_DOWNLOAD_DIR, CVE_MODIFIED_FEED
import os
import xml.etree.ElementTree as xml_parser
import sys

XML_KEY = 'xml'
FILENAME_KEY = 'filename'
NAME_KEY = 'name'
FACT_REF_KEY = 'fact-ref'
LOGICAL_TEST_KEY = 'logical-test'
NAMESPACE_CPE_LANG = '{http://cpe.mitre.org/language/2.0}'
NAMESPACE = '{http://scap.nist.gov/schema/vulnerability/0.4}'
VULNERABLE_SOFTWARE_LIST = 'vulnerable-software-list'
VULNERABLE_CONFIGURATION = 'vulnerable-configuration'
SUMMARY = 'summary'
ZIP_EXTENSION = '.zip'
REJECTED = '** REJECT **'


def read_cve_feeds():
    feeds = []
    for feed_zip_filename in os.listdir(CVE_FEEDS_DOWNLOAD_DIR):
        feed_zip_file_path = os.path.join(CVE_FEEDS_DOWNLOAD_DIR, feed_zip_filename)
        feed_filename = feed_zip_filename.replace(ZIP_EXTENSION, '')
        feed = unzip_cve_feed(feed_zip_file_path, feed_filename)
        feed_xlm = xml_parser.parse(feed)
        add_feed(feed_xlm, feeds, feed_filename)
        feed.close()
    return feeds


def add_feed(feed_xlm, feed_xml_files, feed_filename):
    feed = {XML_KEY: feed_xlm.getroot(), FILENAME_KEY: feed_filename}
    if feed_filename.__contains__(CVE_MODIFIED_FEED):
        feed_xml_files.insert(0, feed)
    else:
        feed_xml_files.append(feed)


def unzip_cve_feed(feed_zip_file_path, feed_filename):
    with ZipFile(feed_zip_file_path) as feed_zip:
        return feed_zip.open(feed_filename)


def get_vulnerable_software_list(cve_entry):
    return cve_entry.find('%s%s' % (NAMESPACE, VULNERABLE_SOFTWARE_LIST))


def get_vulnerable_configurations(cve_entry):
    return cve_entry.findall('%s%s' % (NAMESPACE, VULNERABLE_CONFIGURATION))


def get_summary_from_cve_entry(cve_entry):
    return cve_entry.find('%s%s' % (NAMESPACE, SUMMARY))


def get_cpe_entries_from_cve_entries(cve_entries):
    cpe_entries = []
    for cve_entry in cve_entries:
        cpe_entries.extend(get_cpe_entries_from_cve_entry(cve_entry))
    return set(cpe_entries)


def get_cpe_entries_from_cve_entry(cve_entry):
    cpe_entries = []
    vulnerable_sw = get_vulnerable_software_list(cve_entry)
    vulnerable_configurations = get_vulnerable_configurations(cve_entry)
    cpe_entries.extend(get_cpe_entries_form_vulnerable_software(vulnerable_sw))
    cpe_entries.extend(get_cpe_entries_from_vulnerable_configurations(vulnerable_configurations))
    return set(cpe_entries)


def get_cpe_entries_form_vulnerable_software(vulnerable_software):
    cpe_entries = []
    if (vulnerable_software is not None) and (vulnerable_software.__len__() > 0):
        for cpe in vulnerable_software:
            cpe_entries.append(cpe.text)
    return cpe_entries


def get_cpe_entries_from_vulnerable_configurations(vulnerable_configurations):
    cpe_entries = []
    if (vulnerable_configurations is not None) and (vulnerable_configurations.__len__() > 0):
        for vul_config in vulnerable_configurations:
            for logical_test in vul_config:
                add_cpe_entries_from_logical_test(logical_test, cpe_entries)
    return cpe_entries


def add_cpe_entries_from_logical_test(logical_test, cpe_entries):
    logical_test_children = logical_test.findall('%s%s' % (NAMESPACE_CPE_LANG, LOGICAL_TEST_KEY))
    if logical_test_children.__len__() > 0:
        for logical_test_child in logical_test_children:
            add_cpe_entries_from_logical_test(logical_test_child, cpe_entries)
    else:
        for cpe in logical_test.findall('%s%s' % (NAMESPACE_CPE_LANG, FACT_REF_KEY)):
            cpe_entries.append(cpe.attrib[NAME_KEY])


def contains_cpe_entries(cve_entry):
    vulnerable_sw_list = get_vulnerable_software_list(cve_entry)
    vulnerable_configurations = get_vulnerable_configurations(cve_entry)
    cpe_entries_in_vul_configurations = get_cpe_entries_from_vulnerable_configurations(vulnerable_configurations)
    contains_vulnerable_sw = (vulnerable_sw_list is not None) and (vulnerable_sw_list.__len__() > 0)
    return contains_vulnerable_sw or (cpe_entries_in_vul_configurations.__len__() > 0)


def is_cve_entry_rejected(cve_entry):
    summary = get_summary_from_cve_entry(cve_entry)
    if REJECTED in summary.text:
        return True


def main():
    feeds = read_cve_feeds()
    for feed in feeds:
        for cve in feed.get('xml'):
            cpes = get_cpe_entries_from_cve_entry(cve)
            print(cpes)
        break


if __name__ == '__main__':
    sys.exit(main())
